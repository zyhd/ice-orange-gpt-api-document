

# 冰橙API


### 冰橙GPTT提供与OPENAI官方的API兼容的接口方式，方便国内开发者进行与OPENAI的CHATGPT的接口对接服务

[新版接口文档,支持在线调试，提供覆盖全开发语言的使用范例，点击查看 ](https://apifox.com/apidoc/shared-52fb41cb-0e62-4e34-b06d-f9334d7a0502/api-106414419)
支持包括：java,php,python,go,c,javascript等多个开发语言


[web端请求范例（SSE流式加载）](https://gitee.com/zyhd/bingChengGPT) |
[uniapp端请求范例（SSE流式加载 APP\网页\小程序）](https://gitee.com/zyhd/ice-orange-gpt-api-document/blob/master/components.nvue) |
[意见反馈](https://support.qq.com/product/600324)

<div align="center">
<a href="https://gitee.com/zyhd/ice-orange-gpt-api-document/blob/master/README.md">宽屏预览</a>
</div>


# 1.请求地址：
```
https://yewu.bcwhkj.cn/api/v2.Gptliu/search   
```

# 2.请求方式: POST  

# 3.body格式：json

# 4.请求内容
```
{
       "messages" : [
           {"role":"user","content":"你是谁?"}
        ],
       "model": "gpt-3.5-turbo-0613",  //支持model：gpt-3.5-turbo-0613，gpt-3.5-turbo-16k，gpt-3.5-turbo-16k-0613，gpt-4，gpt-4-0613
       "stream":true    
        //"stream":true //sse 流式请求，边接收边输入内容，用户体验较好，响应速度快
        //"stream":false //或者不写此行  非流式请求，需等待全部内容收到后再输出，速度较慢：

        //其它可选用参数说明：
        //"max_tokens": 2000, //可通过此参数限制每次回复的token最大回复数，以达到限制每次的用量的功能
        //"temperature":0.5  介于 0 和 2 之间。较高的值（如 0.8）将使输出结果更加随机，而较低的值（如 0.2）将使其更加集中和确定性。
        //"n":1 此参数值决定生成多少条结果，使用此参数会快速消耗您的字符数。请谨慎使用并确保对max_tokens和进行合理的设置。
}
```


SSE流式请求与非流式请求的区别：

流式POST请求：内容同步输出，即边接收边输出，响应速度快。

普通POST请求：提交后，需等待OPENAI的CHATGPT官方接口全部内容都接收完成后，服务器这边才会将内容将通过接口将数据提交给前端，速度较慢。



# 5.请求头
```
{
    "Content-Type": application/json,
    "Authorization": "Bearer 这段中文换成你自己的TOKEN（注意前面有个英文空格需保留）"
    //例 "Authorization": "Bearer ****************"
}
 ```

说明：
### token获取方式：

访问公众号《冰橙文化科技》进入菜单冰橙GPT后，访问右上角 / 个人（首次访问免费赠送15000字符，可通过充值增加字符数）可查看 token

通过微信访问：https://yewu.bcwhkj.cn 》 个人 》Token令牌 获取（首次访问赠送15000字符）




也可通过微信扫以下图片进入：

![冰橙GPT](https://bcwhkj.cn/static/upload/image/20230224/1677246223210828.png)


 

# 5.响应体格式：json

# 6.1响应内容（非流式请求时的响应内容）：
```
{
    "id": "chatcmpl-77dQj73rIl0GJyTpAH4QlcSnhOFKp",
    "object": "chat.completion",
    "created": 1682054221,
    "model": "gpt-3.5-turbo-0301",
    "usage": {
        "prompt_tokens": 13,
        "completion_tokens": 30,
        "total_tokens": 43
    },
    "choices": [
        {
            "message": {
                "role": "assistant",
                "content": "我是一个人工智能语言模型，可以根据用户提供的输入进行回答和交流。"
            },
            "finish_reason": "stop",
            "index": 0
        }
    ]
}
```

# 6.2响应内容（流式请求时的响应内容）：
```
data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"role":"assistant"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"我"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"是"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"一个"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"AI"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"语"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"言"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"模"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"型"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"，"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"专"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"门"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"用"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"来"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"回"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"答"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"问题"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"和"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"提"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"供"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"帮"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"助"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"的"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{"content":"。"},"index":0,"finish_reason":null}]}

data: {"id":"chatcmpl-783jPdq1a1ZG6KQc9pq98FDRSqNmp","object":"chat.completion.chunk","created":1682155323,"model":"gpt-3.5-turbo-0301","choices":[{"delta":{},"index":0,"finish_reason":"stop"}]}

data: [DONE]
 
```


# 特别说明【连续对话功能 - 小白入门级方法】：

如果需要有上下文语境，请把之前问的问题和答案一起通过Q参数传过来，其中问题和答案前面分别加上“ Q:” 和 “ A:”参数 ，Q和A前面有空格，举例：

Q = "你好！"

A : 你好，有什么可以帮助你的。

Q: "今天天气怎么样？"

那么第二次提交带上之前的问题和答案，请求体里面的Q就是：

keyword = " Q:你好！ A:你好，有什么可以帮助你的。 Q:今天天气怎么样？"

JS方法如下：
```
keyword='';                
that.list={ 　　
    wen:'老婆和妈妈同时掉水里，我要先救谁？', 　　
    da:'这种情况下，是理所当然应该先救妈妈的。救人的原则是先救生命危险的人，有可能先救老婆，这取决于老婆和妈妈的实际情况，而且也要考虑到逃生的可能性。假如老婆游泳能力很强，可能先救老婆，能在短时间内将她救出水面，否则，先救妈妈也是明智之举。 '
};        

//新问题
newQ:'我不同意？';        

//将之前的所有问答列表进行循环
that.list.forEach((v,k)=>{　　
    //将问和答案组合，<|endoftext|>是用于给OPENAI官方接口进行分段识别的　　
    keyword=keyword+' Q: '+v.wen + ' A: '+v.da+'。 <|endoftext|> ';
})        

//将新的问题组合　　        
keyword=keyword+'Q: '+newQ + 'A: ';

 ```


 
# 特别说明【连续对话功能 - 正经人用法】：

如果需要有上下文语境，请把之前问的问题和答案一起通过messages参数传过来，在其中问题定义好各角色的对话及历史对话，举例：

1、可在开头先设定系统角色或是预设条件：你是一个软件工程师

2、用户首次提问： "你好！你是谁"

3、AI回答 : 我是一个程序工程师。

4、用户再次提问：: "那帮我写个数组排序方法吧！"


JS方法如下：
```
{
       "messages" : [
           {"role":"system","content":"你是一个软件工程师"},
           {"role":"user","content":"你好！你是谁?"},
           {"role":"assistant","content":"我是一个程序工程。"},
           {"role":"user","content":"那帮我写个数组排序方法吧！"}
           {"role":"assistant","content":""}
        ],
       "model": "gpt-3.5-turbo-0613",  //支持model：gpt-3.5-turbo-0613，gpt-3.5-turbo-16k，gpt-3.5-turbo-16k-0613
       "stream":true //非SSE流式请求，这一行可不加
}

 ```
不断在messages里增加序列就可以了。
这样获取的答案就是支持上下文语境了哈～

 



# GPT4请求

请求地址
```
https://yewu.bcwhkj.cn/api/v2.Gptliu/search4  //2023-7-26 更新为可直接使用  https://yewu.bcwhkj.cn/api/v2.Gptliu/search 了，不再需要变更接口地址，采用与3.5同个接口
```


# GPT4请求内容（sse 流式请求，边响应边输出，用户体验效果较好，响应速度快）（非流式请求，可不加stream这一行）：
```
{
       "messages" : [{"role":"user","content":"你是谁?"}],
       "model": "gpt-4-0613",  
       "stream":true //非SSE流式请求，这一行可不加
}
```

其它参数与3.5的一致








# 向量接口：文本转向量

向量数据库介绍:https://zhuanlan.zhihu.com/p/627254037

请求地址
```
https://yewu.bcwhkj.cn/api/v2.Gptliu/embeddings   
```


# 向量请求内容
```
//body请求内容
{
  "model":"text-embedding-ada-002",  
  "input":"你的身份是：腾讯人工智能客服"
}
```
```
//header请求内容
{
    "Content-Type": application/json,
    "Authorization": "Bearer 这段中文换成你自己的TOKEN（注意前面有个英文空格需保留）"
}
```


```
//返回内容
{
    "object": 
    "list",
    "data": [
        {
            "object": 
            "embedding",
            "index": 0,
            "embedding": [...] // 1536 items 
        }
    ],
    "model": "text-embedding-ada-002-v2",
    "usage": {"prompt_tokens": 18,"total_tokens": 18}
}

```
其它参数与3.5的一致






# 查询余额方式1 POST方式

请求地址
```
https://yewu.bcwhkj.cn/api/v2.Gptliu/Balance   
```

请求内容
```
//body请求内容为空即可
```
```
//header请求内容
{
    "Content-Type": application/json,
    "Authorization": "Bearer 这段中文换成你自己的TOKEN（注意前面有个英文空格需保留）"
}
```


```
//返回内容
{
    "codes": 200,
    "mess": "余额查询",
    "data": {
        "count": 460508, //剩余可用字符数
        "name": "授权用户",
        "regTime": "2023-05-08 15:55:34",
        "nowTime": "2023-07-22 17:17:06"
    }
}
```




# 查询余额方式2 Get方式

请求地址
```
https://yewu.bcwhkj.cn/api/v2.Gptliu/Balance?token=这里输入你的TOKEN   

```


```
//返回内容
{
    "codes": 200,
    "mess": "余额查询",
    "data": {
        "count": 460508, //剩余可用字符数
        "name": "授权用户",
        "regTime": "2023-05-08 15:55:34",
        "nowTime": "2023-07-22 17:17:06"
    }
}
```


